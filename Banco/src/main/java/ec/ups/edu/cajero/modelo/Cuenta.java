package ec.ups.edu.cajero.modelo;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cuenta")
public class Cuenta implements Serializable{

	@Id
	private String numCuenta;
	private String cedulaUsuario;
	private String telefono;
	@Column(nullable = false)
	private double saldoCuenta;
	public String getCedulaUsuario() {
		return cedulaUsuario;
	}
	public void setCedulaUsuario(String cedulaUsuario) {
		this.cedulaUsuario = cedulaUsuario;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	public double getSaldoCuenta() {
		return saldoCuenta;
	}
	public void setSaldoCuenta(double saldoCuenta) {
		this.saldoCuenta = saldoCuenta;
	}
	@Override
	public String toString() {
		return "Cuenta [cedulaUsuario=" + cedulaUsuario + ", telefono=" + telefono + ", numCuenta=" + numCuenta
				+ ", saldoCuenta=" + saldoCuenta + "]";
	}
	

	

		

}
