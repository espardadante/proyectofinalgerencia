package ec.ups.edu.cajero.on;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.ups.edu.cajero.dao.CuentaDAO;
import ec.ups.edu.cajero.modelo.Cuenta;
import ec.ups.edu.cajero.modelo.Mensajes;
import ec.ups.edu.cajero.modelo.Recarga;


@Stateless
public class GestionON {
	
	@Inject
	private CuentaDAO cdao;
	 
	public Mensajes debitoCuenta(Recarga re) {
		Mensajes m = new Mensajes();
		Cuenta c = cdao.consultarCuenta(re.getCuenta());
		if(c==null) {
		m.setId(400);
		m.setMensaje("La cuenta no es la correcta");
		return m;
		}else if(c.getSaldoCuenta()<re.getRecarga()) {
			m.setId(400);
			m.setMensaje("Saldo de cuentra incorrecto");
			return m;
		}else {
			c.setSaldoCuenta(c.getSaldoCuenta()-re.getRecarga());
			cdao.actualizarCuenta(c);
			m.setId(200);
			m.setMensaje("Debito correcto");
			return m;
		}
		
	}

}
