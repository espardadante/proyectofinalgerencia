package ec.ups.edu.cajero.servicios;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import ec.ups.edu.cajero.modelo.Mensajes;
import ec.ups.edu.cajero.modelo.Recarga;
import ec.ups.edu.cajero.on.GestionON;

@Path("/servicios")
public class serviciosREST {	
	@Inject
	private GestionON con;	
 
	@POST
	@Path("/debito")
	@Produces("application/json")
	public Mensajes realizarDebito(Recarga r) {
		
		Mensajes m = con.debitoCuenta(r);
		return m;
	}	
}
