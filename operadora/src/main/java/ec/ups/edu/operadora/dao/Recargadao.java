package ec.ups.edu.operadora.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ec.ups.edu.operadora.modelo.Recarga;

@Stateless
public class Recargadao {
	
	@PersistenceContext
	private EntityManager em;

	/**
	 * metodo crear cuenta
	 * 
	 * @param cuenta recibe como paremetro el objeto cuenta
	 */
	public boolean crearRecarga(Recarga r) {
		try {
			em.persist(r);
			return true;
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
		
	}
}
