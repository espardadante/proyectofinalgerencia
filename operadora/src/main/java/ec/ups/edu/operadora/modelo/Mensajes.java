package ec.ups.edu.operadora.modelo;

import java.io.Serializable;

public class Mensajes implements Serializable{

	private int id;
	private String mensaje;
	
	public Mensajes() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	@Override
	public String toString() {
		return "Mensajes [id=" + id + ", mensaje=" + mensaje + "]";
	}
	
	
}
