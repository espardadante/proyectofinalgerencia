package ec.ups.edu.operadora.modelo;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "recarga")
public class Recarga implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String cedula;
	private String telefono;
	private double recarga;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public double getRecarga() {
		return recarga;
	}
	public void setRecarga(double recarga) {
		this.recarga = recarga;
	}
	
	@Override
	public String toString() {
		return "Recarga [id=" + id + ", cedula=" + cedula + ", telefono=" + telefono + ", recarga=" + recarga + "]";
	}
	
}
