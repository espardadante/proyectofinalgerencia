package ec.ups.edu.operadora.on;

import java.util.Random;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.ups.edu.operadora.dao.Recargadao;
import ec.ups.edu.operadora.modelo.Mensajes;
import ec.ups.edu.operadora.modelo.Recarga;

@Stateless
public class GestionON {
	@Inject
	private Recargadao rdao;
	
	
	
	
	public Mensajes guardarRecargar(Recarga r) {
		rdao.crearRecarga(r);
		Mensajes m = new Mensajes();
		m.setId(200);
		m.setMensaje("Se guardo correctamente");
		return m;
		
	}
	
	public Mensajes solicitud() {
		Mensajes m = new Mensajes();
        Random ran = new Random();
		int valor = ran.nextInt(2);
		System.out.println(valor);
		if(valor ==0) {
			m.setId(200);
			m.setMensaje("Se acepta la solicitud");
			return m;
		}else {
			m.setId(400);
			m.setMensaje("solicitud negada");
			return m;
		}
	}
}
