package ec.ups.edu.operadora.servicios;

import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ec.ups.edu.operadora.modelo.Mensajes;
import ec.ups.edu.operadora.modelo.Recarga;
import ec.ups.edu.operadora.on.GestionON;


@Path("/servicios")
public class serviciosREST {	
	@Inject
	private GestionON con;	
 
	@POST
	@Path("/solicitar")
	@Produces("application/json")
	public Mensajes S() {
		
		Mensajes m = con.solicitud();
		return m;
	}
	@POST
	@Path("/guardarRecarga")
	@Produces("application/json")
	public Mensajes guardarSolicitud(Recarga r) {
		Mensajes m = con.guardarRecargar(r);
		return m;
		
	}
}
