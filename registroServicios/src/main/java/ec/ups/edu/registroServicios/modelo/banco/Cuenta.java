package ec.ups.edu.registroServicios.modelo.banco;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class Cuenta implements Serializable{

	@Id
	private String numCuenta;
	private String cedulaUsuario;
	private String telefono;
	private double saldoCuenta;
	
	public String getCedulaUsuario() {
		return cedulaUsuario;
	}
	public void setCedulaUsuario(String cedulaUsuario) {
		this.cedulaUsuario = cedulaUsuario;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	public double getSaldoCuenta() {
		return saldoCuenta;
	}
	public void setSaldoCuenta(double saldoCuenta) {
		this.saldoCuenta = saldoCuenta;
	}
	@Override
	public String toString() {
		return "Cuenta [cedulaUsuario=" + cedulaUsuario + ", telefono=" + telefono + ", numCuenta=" + numCuenta
				+ ", saldoCuenta=" + saldoCuenta + "]";
	}
	

	

		

}
