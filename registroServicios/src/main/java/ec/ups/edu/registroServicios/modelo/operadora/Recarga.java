package ec.ups.edu.registroServicios.modelo.operadora;

import java.io.Serializable;

public class Recarga implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String cedula;
	private String cuenta;
	private String telefono;
	private double recarga;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public double getRecarga() {
		return recarga;
	}
	public void setRecarga(double recarga) {
		this.recarga = recarga;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	@Override
	public String toString() {
		return "Recarga [id=" + id + ", cedula=" + cedula + ", cuenta=" + cuenta + ", telefono=" + telefono
				+ ", recarga=" + recarga + "]";
	}
			

}
