package ec.ups.edu.registroServicios.modelo.operadora;

import java.io.Serializable;

public class Sms implements Serializable{

	private String uudi;
	private String numero;
	
	public String getUudi() {
		return uudi;
	}
	public void setUudi(String uudi) {
		this.uudi = uudi;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	@Override
	public String toString() {
		return "Sms [uudi=" + uudi + ", numero=" + numero + "]";
	}
	
	
	
}
