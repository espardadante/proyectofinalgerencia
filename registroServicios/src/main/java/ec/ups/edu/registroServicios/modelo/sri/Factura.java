package ec.ups.edu.registroServicios.modelo.sri;

import java.io.Serializable;

public class Factura implements Serializable{
	
	private String uuid;
	private String fecha;
	private String cedula;
	private double valor;
	
	
	



	public String getUuid() {
		return uuid;
	}



	public void setUuid(String uuid) {
		this.uuid = uuid;
	}



	public String getFecha() {
		return fecha;
	}



	public void setFecha(String fecha) {
		this.fecha = fecha;
	}



	public String getCedula() {
		return cedula;
	}



	public void setCedula(String cedula) {
		this.cedula = cedula;
	}



	public double getValor() {
		return valor;
	}



	public void setValor(double valor) {
		this.valor = valor;
	}



	@Override
	public String toString() {
		return "Factura [uuid=" + uuid + ", fecha=" + fecha + ", cedula=" + cedula + ", valor=" + valor + "]";
	}



	
}
