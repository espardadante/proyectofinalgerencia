package ec.ups.edu.registroServicios.on;

import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;

import javax.ws.rs.client.WebTarget;

import ec.ups.edu.registroServicios.modelo.global.Mensajes;
import ec.ups.edu.registroServicios.modelo.operadora.Recarga;
import ec.ups.edu.registroServicios.modelo.operadora.Sms;
import ec.ups.edu.registroServicios.modelo.sri.Factura;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

@Stateless
public class ClienteRegistroServicios {

	public String WS_POST_OPERADORA_SOLICITAR = "http://localhost:8080/operadora/ws/servicios/solicitar";
	public String WS_POST_OPERADORA_GUARDAR = "http://localhost:8080/operadora/ws/servicios/guardarRecarga";
	public String WS_POST_SRI_GUARDAR = "http://localhost:8080/sri/ws/servicios/guardar";
	public String WS_POST_BANCO_DEBITO = "http://localhost:8080/Banco/ws/servicios/debito";
	public String WS_GET_SRI_FACTURA = "http://localhost:8080/sri/ws/servicios/factura";
	public String WS_GET_SRI_SOLICITUD = "http://localhost:8080/sri/ws/servicios/solicitar";
	public Mensajes solicitudOperadora() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_POST_OPERADORA_SOLICITAR);
		Mensajes respuesta = target.request().post(null,Mensajes.class);
		return respuesta;

	}

	public Mensajes guardarOperadora(Recarga recarga) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_POST_OPERADORA_GUARDAR);
		Mensajes respuesta = target.request().post(Entity.json(recarga), Mensajes.class);
		return respuesta;

	}

	public Mensajes solicitarSRI() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_SRI_SOLICITUD);
		Mensajes respuesta = target.request().post(null, Mensajes.class);
		return respuesta;

	}
	public Mensajes realizarDebito (Recarga recarga) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_POST_BANCO_DEBITO);
		Mensajes respuesta = target.request().post(Entity.json(recarga), Mensajes.class);
		return respuesta;

	}

	public Factura getFactura(String id) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_SRI_FACTURA).queryParam("id", id);
		Factura factura = target.request().get(Factura.class);
		client.close();
		return factura;
	}
	public Mensajes generarFactura (Factura recarga) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_POST_SRI_GUARDAR);
		Mensajes respuesta = target.request().post(Entity.json(recarga), Mensajes.class);
		return respuesta;

	}
	
	public Mensajes enviarSms(Sms sms) {
		Mensajes m = new Mensajes();
		HttpResponse<String> response = Unirest.post("https://api.labsmobile.com/json/send")
				  .header("Content-Type", "application/json")
				  .basicAuth("jviscainoq@est.ups.edu.ec","y2ZTWO65izDnPEm2pJA9JFAKtE1hc5bA")
				  .header("Cache-Control", "no-cache")
				  .body("{\"message\":\"Su factura electronica "+sms.getUudi() +"se ha generado conforme a la transaccion realizada. Ud puede revisarla en nuestra pagina \", \"tpoa\":\"Sender\",\"recipient\":[{\"msisdn\":\"593"+sms.getNumero()+"\"}]}")
				  .asString();
			System.out.println(response.getBody());
			System.out.println(response.getStatus());
			if(response.getStatus()==200) {
				m.setId(200);
				m.setMensaje("envio Ok");
				return m;
			}else {
				m.setId(400);
				m.setMensaje("envio erroneo");
				return m;
			}
	}
	
}
