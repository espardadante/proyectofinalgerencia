package ec.ups.edu.registroServicios.on;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.ups.edu.registroServicios.modelo.global.Mensajes;
import ec.ups.edu.registroServicios.modelo.operadora.Recarga;
import ec.ups.edu.registroServicios.modelo.operadora.Sms;
import ec.ups.edu.registroServicios.modelo.sri.Factura;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

@Stateless
public class GestionON {
	@Inject
	private ClienteRegistroServicios serv;

	public Mensajes restSolicitudOperadora() {
		return serv.solicitudOperadora();

	}

	public Mensajes restGuardarDatosOperadora(Recarga recarga) {
		return serv.guardarOperadora(recarga);

	}

	public Mensajes restSolicitudSRI() {
		return serv.solicitarSRI();
	}

	public Mensajes restBancoDebito(Recarga recarga) {
		return serv.realizarDebito(recarga);
	}
	
	public Factura restFactura(String id) {
		return serv.getFactura(id);
	}
	
	public Mensajes restGenerarFactura(Factura factura) {
		return serv.generarFactura(factura);
	}
	
	public Mensajes enviarSms(Sms sms) {
		
		return serv.enviarSms(sms);
	}
}