package ec.ups.edu.registroServicios.servicios;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import ec.ups.edu.registroServicios.modelo.global.Mensajes;
import ec.ups.edu.registroServicios.modelo.operadora.Recarga;
import ec.ups.edu.registroServicios.modelo.operadora.Sms;
import ec.ups.edu.registroServicios.modelo.sri.Factura;
import ec.ups.edu.registroServicios.on.GestionON;



@Path("/servicios")
public class serviciosREST {	
	@Inject
	private GestionON con;	
 
	@POST
	@Path("/solicitarOperadora")
	@Produces("application/json")
	public Mensajes operadoraSolicitudRecarga() {
		
		Mensajes m = con.restSolicitudOperadora();
		return m;
	}
	
	@POST
	@Path("/guardarRecargaOperadora")
	@Produces("application/json")
	public Mensajes operadoraGuardarRecarga(Recarga r) {
		Mensajes m = con.restGuardarDatosOperadora(r);
		return m;
		
	}
	@POST
	@Path("/solicitarSRI")
	@Produces("application/json")
	public Mensajes solicitudSri() {
		Mensajes m = con.restSolicitudSRI();
		return m;
		
	}
	@POST
	@Path("/guardarSRI")
	@Produces("application/json")
	public Mensajes sriGenerarFactura(Factura f) {
		Mensajes m = con.restGenerarFactura(f);
		return m;
		
	}

	@GET
	@Path("/facturaSri")
	@Produces("application/json")
	public Factura getCategoria(@QueryParam("id") String id) {
		System.out.println(id);
		return con.restFactura(id);
		
	}
	@POST
	@Path("/bancoDebito")
	@Produces("application/json")
	public Mensajes bancoDebito(Recarga r) {
		Mensajes m = con.restBancoDebito(r);
		return m;
		
	}
	@POST
	@Path("/enviarsms")
	@Produces("application/json")
	public Mensajes enviarSms(Sms sms ) {
		Mensajes m =con.enviarSms(sms);
		return m;
		
	}
	
	
}
