package ec.ups.edu.sri.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ec.ups.edu.sri.modelo.Factura;
@Stateless
public class FacturaDAO {
	
	@PersistenceContext
	private EntityManager em;

	/**
	 * metodo crear cuenta
	 * 
	 * @param cuenta recibe como paremetro el objeto cuenta
	 */
	public boolean crearFactura(Factura f) {
		try {
			em.persist(f);
			return true;
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		}
		
	}
	
	public Factura ConsularFactura(String UUID) {
		return em.find(Factura.class, UUID);
	}
	
	
	
	
	
}
