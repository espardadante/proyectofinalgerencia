package ec.ups.edu.sri.on;

import java.util.Random;

import javax.ejb.Stateless;
import javax.inject.Inject;
import ec.ups.edu.sri.dao.FacturaDAO;
import ec.ups.edu.sri.modelo.Factura;
import ec.ups.edu.sri.modelo.Mensajes;

@Stateless
public class GestionON {
	@Inject
	private FacturaDAO rdao;
	
	
	public String generaUUID() {
		char[] chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		int charsLength = chars.length;
		Random random = new Random();
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < 32; i++) {
			buffer.append(chars[random.nextInt(charsLength)]);
		}
		System.out.println("password " + buffer.toString());
		return buffer.toString();
	}
	
	public void guardarRegistro(Factura f) {
		rdao.crearFactura(f);
		
	}
	
	public Mensajes solicitudRegistro() {
		Mensajes m = new Mensajes();

        Random ran = new Random();
		int valor = ran.nextInt(2);
		System.out.println(valor);
		if(valor ==0) {
			m.setId(200);
			m.setMensaje("Se acepta la solicitud");
			return m;
		}else {
			m.setId(400);
			m.setMensaje("Solicitud negada");
			return m;
		}
	}

	public Mensajes generaFactura(Factura factura) {
		factura.setUuid(generaUUID());
		rdao.crearFactura(factura);
		Mensajes m =new Mensajes();
		m.setId(200);
		m.setMensaje(factura.getUuid());
		return m;
	}
	
	public Factura buscarFactura(String UUID) {
		Factura f=rdao.ConsularFactura(UUID);
		return f;
	}
}
