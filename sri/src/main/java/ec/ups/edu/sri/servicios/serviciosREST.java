package ec.ups.edu.sri.servicios;

import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ec.ups.edu.sri.modelo.Factura;
import ec.ups.edu.sri.modelo.Mensajes;
import ec.ups.edu.sri.on.GestionON;

@Path("/servicios")
public class serviciosREST {	
	@Inject
	private GestionON con;	
 
	@POST
	@Path("/solicitar")
	@Produces("application/json")
	public Mensajes realizarSolicitud() {
		Mensajes m = con.solicitudRegistro();
		return m;
	}
	@POST
	@Path("/guardar")
	@Produces("application/json")
	public Mensajes guardarFactura(Factura factura) {
		
		return con.generaFactura(factura);
	}
	
	@GET
	@Path("/factura")
	@Produces("application/json")
	public Factura getCategoria(@QueryParam("id") String id) {
		System.out.println(id);
		Factura fva= new Factura();
		
		Factura f = con.buscarFactura(id);
		if(f==null) {
			System.err.println(fva);
			return fva;
			
		}else {
			System.err.println(f);
			return f;
		}
		
	}
}
